/**
 * @file main_window.hpp
 * @author Alexander Bogoslovskiy (bogoslovsky.alexander@ya.ru)
 * @brief GUI and funtionality class declaration
 * @version 1.0.0
 * @date 2022-03-23
 *
 * @copyright Copyright (c) 2022
 *
 */

#pragma once

#include "common.hpp"

/// Time period between costmap data updates (seconds)
constexpr auto COSTMAP_CHECK_PERIOD = 5;

/// Time period after last costmap data update to check collisions with
/// footprint polygon (seconds)
constexpr auto COSTMAP_POLYGON_CROSS_PERIOD = 0.5;

class MainWin : public QMainWindow {
  Q_OBJECT

 public:
  /// Get GUI object references, establish signal-slot connections
  MainWin(QWidget *parent);

  /// Delete heap allocated objects
  ~MainWin();

 private:
  /**
   * @brief rosbag parser
   * @param bag rosbag empty object pointer to load data
   * @return true rosbag sucessfuly loaded
   * @return false error occured during rosbag loading
   */
  auto parseRosBag(rosbag::Bag *bag) -> bool;

  /**
   * @brief Check collisions between robot footprint and occupied map zones
   * @param bag rosbag file with data recordings
   */
  void collisionCheck(rosbag::Bag *bag);

  /**
   * @brief Check SLAM errors
   * @param bag rosbag file with data recordings
   */
  void slamCheck(rosbag::Bag *bag);

  /**
   * @brief Check Move Base errors
   * @param bag rosbag file with data recordings
   */
  void moveBaseCheck(rosbag::Bag *bag);

  /**
   * @brief Update costmap object with occupancy data from rosbag
   * @param costmap Costmap object
   * @param data Occupancy cell data
   */
  void updCostmapFromMsg(costmap_2d::Costmap2D *costmap,
                         std::vector<int8_t, std::allocator<int8_t>> *data);

  /**
   * @brief Turn matrix double index into array index
   * @param x Row matrix index
   * @param y Column matrix
   * @param colN Number of matrix columns
   * @return quint32 Array index
   */
  auto idxFromMatrixToArr(quint32 x, quint32 y, quint32 colN) -> quint32 {
    return x * colN + y;
  };

  /**
   * @brief Turn array index into matrix double index
   * @param idx Array index
   * @param colN Number of matrix columns
   * @return std::vector<quint32> Matrix double index
   */
  auto idxFromArrToMatrix(quint32 idx, quint32 colN) -> std::vector<quint32> {
    std::vector<quint32> matrixIdx = {0, 0};
    matrixIdx[0] = idx / colN;
    matrixIdx[1] = idx % colN;
    return matrixIdx;
  };

  /**
   * @brief Set the Output object
   * @param resTxtLst
   */
  void setOutput(QStringList *resTxtLst);

 private slots:

  /// Qt GUI slot to look for rosbag file using standart windowed finder
  void findFileSlot();

  /// Qt GUI slot to start process of looking of sysem issues in the given
  /// rosbag
  void getResSlot();

 private:
  /// Main GUI object reference
  Ui::MainWindow *ui_ = Q_NULLPTR;

  /// Reference to the file path line edit object
  QLineEdit *fPathLEdit_ = Q_NULLPTR;

  /// Find file button object reference
  QPushButton *findFilePBtn_ = Q_NULLPTR;

  /// Radio button object reference array to choose error check mode
  QList<QRadioButton *> radioBtnLst_;

  /// Get result button object reference
  QPushButton *getResPBtn_ = Q_NULLPTR;

  /// Result output field object reference
  QTextEdit *resTEdit_ = Q_NULLPTR;

  /// Rosbag group object reference
  QGroupBox *bagGBox_ = Q_NULLPTR;

  /// Checkbox group object reference
  QGroupBox *checkGBox_ = Q_NULLPTR;

  /// Costmap object reference
  costmap_2d::Costmap2D *costmap_ = Q_NULLPTR;

  /// Was costmap object initialized or not
  bool costmapInit_ = false;

  /// Was collision found during last time period or not
  bool costmapCollideReport_ = false;

  /// Last time when costmap updated
  ros::Time costmapSyncTime_;
};
