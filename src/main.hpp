/**
 * @file main.hpp
 * @author Alexander Bogoslovskiy (bogoslovsky.alexander@ya.ru)
 * @brief Program entry point function declaration
 * @version 1.0.0
 * @date 2022-03-23
 *
 * @copyright Copyright (c) 2022
 *
 */

#pragma once

#include "common.hpp"
#include "main_window.hpp"

auto main(qint32 argc, char *argv[]) -> qint32;
