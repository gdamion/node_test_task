/**
 * @file main.cpp
 * @author Alexander Bogoslovskiy (bogoslovsky.alexander@ya.ru)
 * @brief Program entry point function definiton
 * @version 1.0.0
 * @date 2022-03-23
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "main.hpp"

auto main(qint32 argc, char *argv[]) -> qint32 {
  QApplication app(argc, argv);
  QCoreApplication::setApplicationName("NodeAnalyzer");
  QCoreApplication::setApplicationVersion("1.0");

  auto mainWin = new MainWin(Q_NULLPTR);
  mainWin->show();

  auto state = app.exec();
  mainWin->deleteLater();
  return state;
}
