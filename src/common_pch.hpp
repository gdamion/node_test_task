/**
 * @file common_pch.hpp
 * @author Alexander Bogoslovskiy (bogoslovsky.alexander@ya.ru)
 * @brief Include files to precompile
 * @version 1.0.0
 * @date 2022-03-23
 *
 * @copyright Copyright (c) 2022
 *
 */

/// Qt modules
#include <QtCore>
#include <QtGui>
#include <QtWidgets>

/// ROS modules
#include <actionlib_msgs/GoalID.h>
#include <actionlib_msgs/GoalStatusArray.h>
#include <costmap_2d/costmap_2d.h>
#include <geometry_msgs/PolygonStamped.h>
#include <map_msgs/OccupancyGridUpdate.h>
#include <mbf_msgs/ExePathActionFeedback.h>
#include <mbf_msgs/ExePathActionResult.h>
#include <nav_msgs/OccupancyGrid.h>
#include <ros/ros.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <std_msgs/String.h>

/// STD modules
#include <iostream>
#include <string>
#include <vector>

/// Boost time classes
#include <boost/date_time/posix_time/posix_time.hpp>
