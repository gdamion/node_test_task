/**
 * @file main_window.cpp
 * @author Alexander Bogoslovskiy (bogoslovsky.alexander@ya.ru)
 * @brief GUI and funtionality class definition
 * @version 1.0.0
 * @date 2022-03-23
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "main_window.hpp"

MainWin::MainWin(QWidget *parent)
    : QMainWindow(parent), ui_(new Ui::MainWindow) {
  ui_->setupUi(this);

  fPathLEdit_ = ui_->fPathLEdit;
  findFilePBtn_ = ui_->findFilePBtn;
  radioBtnLst_ = {ui_->radioButton1, ui_->radioButton2, ui_->radioButton3};
  getResPBtn_ = ui_->getResPBtn;
  resTEdit_ = ui_->resTEdit;
  bagGBox_ = ui_->bagGBox;
  checkGBox_ = ui_->checkGBox;

  connect(findFilePBtn_, &QPushButton::clicked, this, &MainWin::findFileSlot);
  connect(getResPBtn_, &QPushButton::clicked, this, &MainWin::getResSlot);
}

MainWin::~MainWin() {
  if (costmap_) {
    delete costmap_;
  }
}

void MainWin::findFileSlot() {
  QString fileName = QFileDialog::getOpenFileName(
      this, tr("Open File"), "/home", tr("ROS bag files (*.bag)"));

  if (fileName != Q_NULLPTR) {
    fPathLEdit_->setText(fileName);
    resTEdit_->clear();
  } else {
    QString warnStr = "File browse result is equal to an empty string";
    qWarning().noquote() << warnStr;
  }
}

void MainWin::getResSlot() {
  resTEdit_->clear();

  rosbag::Bag bag;
  if (!parseRosBag(&bag)) {
    return;
  }

  if (radioBtnLst_[0]->isChecked()) {
    collisionCheck(&bag);
  } else if (radioBtnLst_[1]->isChecked()) {
    slamCheck(&bag);
  } else {
    moveBaseCheck(&bag);
  }

  bag.close();
}

auto MainWin::parseRosBag(rosbag::Bag *bag) -> bool {
  try {
    bag->open(fPathLEdit_->text().toStdString(), rosbag::bagmode::Read);
  } catch (...) {
    QString errStr = "Couldn't load bag file";
    qCritical().noquote() << errStr;
    resTEdit_->setText(errStr);
    return false;
  }
  return true;
}

void MainWin::collisionCheck(rosbag::Bag *bag) {
  std::vector<std::string> topics;
  std::string ns = "/move_base/local_costmap/";
  std::string footprint = ns + "footprint";
  std::string costmap = ns + "costmap";
  std::string costmapUpd = ns + "costmap_updates";

  topics.push_back(footprint);
  topics.push_back(costmap);
  topics.push_back(costmapUpd);

  rosbag::View view(*bag, rosbag::TopicQuery(topics));

  QStringList resTxtLst;

  for (rosbag::MessageInstance const m : view) {
    auto time = m.getTime();
    auto my_posix_time = time.toBoost();
    auto iso_time_str =
        boost::posix_time::to_iso_extended_string(my_posix_time);

    auto topic = m.getTopic();
    if (topic == costmap) {
      auto s = m.instantiate<nav_msgs::OccupancyGrid>();

      if (s != Q_NULLPTR) {
        if (!costmapInit_) {
          costmap_ = new costmap_2d::Costmap2D(
              s->info.width, s->info.height, s->info.resolution,
              s->info.origin.position.x, s->info.origin.position.y, 0);
          costmapInit_ = true;
          updCostmapFromMsg(costmap_, &s->data);
          costmapSyncTime_ = time;
        }

        ros::Duration timeDiff = time - costmapSyncTime_;
        if (timeDiff.toSec() > COSTMAP_CHECK_PERIOD) {
          updCostmapFromMsg(costmap_, &s->data);
          costmapSyncTime_ = time;
          costmapCollideReport_ = false;
        }
      }
    } else if (topic == costmapUpd && costmapInit_) {
      auto s = m.instantiate<map_msgs::OccupancyGridUpdate>();

      if (s != Q_NULLPTR) {
        ros::Duration timeDiff = time - costmapSyncTime_;
        if (timeDiff.toSec() > COSTMAP_CHECK_PERIOD) {
          updCostmapFromMsg(costmap_, &s->data);
          costmapSyncTime_ = time;
          costmapCollideReport_ = false;
        }
      }
    } else if (topic == footprint && costmapInit_) {
      auto s = m.instantiate<geometry_msgs::PolygonStamped>();

      if (s != Q_NULLPTR && !costmapCollideReport_) {
        ros::Duration timeDiff = time - costmapSyncTime_;
        if (timeDiff.toSec() < COSTMAP_POLYGON_CROSS_PERIOD) {
          std::vector<costmap_2d::MapLocation> footprintPolygon;
          for (auto point : s->polygon.points) {
            costmap_2d::MapLocation costmapPoint;
            costmapPoint.x = point.x;
            costmapPoint.y = point.y;
            footprintPolygon.push_back(costmapPoint);
          }
          std::vector<costmap_2d::MapLocation> outlinePoints;
          costmap_->polygonOutlineCells(footprintPolygon, outlinePoints);

          for (auto point : outlinePoints) {
            auto cost = costmap_->getCost(point.x, point.y);

            if (cost >= 95) {
              auto errStr = QString("%1: Possible collision")
                                .arg(QString::fromStdString(iso_time_str));
              resTxtLst.push_back(errStr);
              costmapCollideReport_ = true;
              break;
            }
          }
        }
      }
    }
  }
  setOutput(&resTxtLst);

  delete costmap_;
  costmap_ = Q_NULLPTR;
  costmapInit_ = false;
  costmapCollideReport_ = false;
}

void MainWin::slamCheck(rosbag::Bag *bag) {
  std::vector<std::string> topics;
  topics.push_back(std::string("/long_term_slam/error_code"));
  rosbag::View view(*bag, rosbag::TopicQuery(topics));

  QStringList resTxtLst;

  for (rosbag::MessageInstance const m : view) {
    boost::posix_time::ptime my_posix_time = m.getTime().toBoost();
    std::string iso_time_str =
        boost::posix_time::to_iso_extended_string(my_posix_time);

    std_msgs::String::ConstPtr s = m.instantiate<std_msgs::String>();
    if (s != Q_NULLPTR) {
      resTxtLst.push_back(QString("%1: %2")
                              .arg(QString::fromStdString(iso_time_str))
                              .arg(QString::fromStdString(s->data)));
    }
  }
  setOutput(&resTxtLst);
}

void MainWin::moveBaseCheck(rosbag::Bag *bag) {
  std::vector<std::string> topics;
  std::string mbNs = "/move_base/exe_path/";
  std::string mbCancel = mbNs + "cancel";
  std::string mbRes = mbNs + "result";

  topics.push_back(mbCancel);
  topics.push_back(mbRes);

  rosbag::View view(*bag, rosbag::TopicQuery(topics));

  QStringList resTxtLst;

  for (rosbag::MessageInstance const m : view) {
    boost::posix_time::ptime my_posix_time = m.getTime().toBoost();
    std::string iso_time_str =
        boost::posix_time::to_iso_extended_string(my_posix_time);

    auto topic = m.getTopic();
    if (topic == mbCancel) {
      resTxtLst.push_back(QString("%1: %2")
                              .arg(QString::fromStdString(iso_time_str))
                              .arg("Cancel path"));
    } else if (topic == mbRes) {
      mbf_msgs::ExePathActionResult::ConstPtr s =
          m.instantiate<mbf_msgs::ExePathActionResult>();

      if (s != Q_NULLPTR && s->result.outcome >= 100) {
        resTxtLst.push_back(QString("%1: %2 (%3)")
                                .arg(QString::fromStdString(iso_time_str))
                                .arg(QString::fromStdString(s->result.message))
                                .arg(s->result.outcome));
      }
    }
  }
  setOutput(&resTxtLst);
}

void MainWin::updCostmapFromMsg(
    costmap_2d::Costmap2D *costmap,
    std::vector<int8_t, std::allocator<int8_t>> *data) {
  quint32 idx = 0;
  for (auto cost : *data) {
    auto matrixIdx = idxFromArrToMatrix(idx, costmap->getSizeInCellsY());
    costmap->setCost(matrixIdx[0], matrixIdx[1], (unsigned char)cost);
    idx++;
  }
}

void MainWin::setOutput(QStringList *resTxtLst) {
  QString resTxt;
  if (resTxtLst->isEmpty()) {
    resTxt = "No errors detected";
  } else {
    resTxt = resTxtLst->join("\n");
  }
  resTEdit_->setText(resTxt);
}
