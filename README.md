# Node Robotics Preparation Task

## Task 1: Error Analyzing

### Malfunction detection

Date and time: 2020/10/01 - somewhere 11:30

Rosbag file: *dummy_env-agv-50231.agv-2020-10-01T082312+0200_2020-10-01-11-28-17_37.bag* - 94th second

**! For some reason given rosbag files don't correlate with the information in the given problem statement file. It was said that issue with the robot occured on October 2nd, but there are only bags for October 1st and earlier. Maybe it's the 24-hour time lag of the robot's OS!**

After a smooth movement along a rectilinear trajectory, the robot abruptly began to spin around its Z axis, while losing angular localization. Supposed reason of spinning: wet floor (oil, water etc), some kind of outer wheel controller node, which published false commands to the *cmd_vel* topics.
At the end of this rotation, the robot ended up in the prohibited zone of a third-party object, which can be interpreted as a collision. First 20-30 seconds there were false prohibition point artifacts on the map, and planner could'n create global path to the goal, therefore move base goal was cancelled. In the same time SLAM module published error, and maybe stopped mapping. After some time the goal was resent to the move base module, but its node reported that the robot was stuck. Robot couldn't leave this position with the standart local planner configuration. None of the coordinate transform chain issues wasn't detected.

[**Rosbag data visualization**](https://youtu.be/HEx9JyBL0_I)

Final short verdict: robot was stuck because of short localization loss.

### Possible problem resolution

* Integrate collision recovery behavior into navigation stack:
  * Clear costmap from trash occupancy zones
  * Generate shortest trajectory to leave occupied zone, including option of moving backward
* Release the recommendation for a customer to keep floors clean and dry

### Instruments used

* RViz
* ROS Bash CLI
* Rqt_bag
* Rqt_tf_tree

## Task 2: Automatic Error Detection

[**Online documentation**](https://gdamion.gitlab.io/node_test_task)

### Detection approaches

According to research in previous task, there are several ways to detect that robot is stuck or lost:
* Define that robot's footprint contains costmap pixels with values 253-254 for a decent amount of time (5-10 seconds or more)

  Topics to use (message type in brackets):
  * */move_base/local_costmap/footprint* (geometry_msgs/PolygonStamped)
  * */move_base/local_costmap/costmap* (nav_msgs/OccupancyGrid)
  * */move_base/local_costmap/costmap_updates* (nav_msgs/OccupancyGridUpdate)

**! This part works, but code/logic look like wrong - result data about possible collisions is unreliable !**

* Listen to the Move Base module errors

  Topics to use (message type in brackets):
  * */move_base/exe_path/cancel* (actionlib_msgs/GoalID)
  * */move_base/exe_path/result* (mbf_msgs/ExePathActionResult) **! Can't be parsed by rosbag API !**

* Listen to the SLAM module errors

  Topics to use (message type in brackets):
  * */long_term_slam/error_code* (std_msgs/String)

### Functionality of the program

* Analyze the selected rosbag: the match of the costmap and restricted areas, move base errors, slam errors
* GUI
  * Field to select rosbag file from filesystem
  * Button to load rosbag file
  * Checkboxes to choose what we are testing: at least 1 must be checked
  * Check run button
  * Output field for check result information
    * can be displayed line by line
    * it is better to keep the chronological order

### Dependencies

* C++ 20
* Qt6 (Core, Gui, Wiegets modules)
* ROS Noetic (roscpp, rosbag, costmap_2d, std_msgs, geometry_msgs, nav_msgs, map_msgs, actionlib_msgs, mbf_msgs)
* Cmake (verion 3.21 and higher)
* Ninja or Make

### Build

Execute this from project root:

```Bash
cmake --preset=ninja-release && cmake --build --preset=ninja-release --parallel 8 # for ninja
cmake --preset=make-release && cmake --build --preset=make-release --parallel 8 # for make
```

### Run

```Bash
<project root>/build/ninja-release/node_analyzer # for ninja
<project root>/build/make/node_analyzer # for make
```

### Interface images

![Image 1](./doc/image_2022-03-22_15-06-12.png)
![Image 2](./doc/image_2022-03-23_21-38-00.png)
![Image 3](./doc/image_2022-03-23_21-38-01-1.png)
![Image 4](./doc/image_2022-03-23_21-38-01.png)
