option(ENABLE_DOXYGEN "Enable doxygen doc builds of source" OFF)

if(ENABLE_DOXYGEN)
  add_subdirectory(${PROJECT_SOURCE_DIR}/doc)
endif()
