option(ENABLE_LINKER "Enable a specific linker if available" ON)

include(CheckCXXCompilerFlag)

set(SET_LINKER
    "lld"
    CACHE STRING "Linker to be used")
set(SET_LINKER_VALUES "lld" "gold" "bfd")
set_property(CACHE SET_LINKER PROPERTY STRINGS ${SET_LINKER_VALUES})
list(FIND SET_LINKER_VALUES ${SET_LINKER} SET_LINKER_INDEX)

if(${SET_LINKER_INDEX} EQUAL -1)
  message(
    STATUS
      "Using custom linker: '${SET_LINKER}', explicitly supported entries are ${SET_LINKER_VALUES}"
  )
endif()

function(configure_linker project_name)
  if(NOT ENABLE_LINKER)
    return()
  endif()

  set(LINKER_FLAG "-fuse-ld=${SET_LINKER}")

  check_cxx_compiler_flag(${LINKER_FLAG} CXX_SUPPORTS_USER_LINKER)
  if(CXX_SUPPORTS_USER_LINKER)
    target_compile_options(${project_name} INTERFACE ${LINKER_FLAG})
  endif()
endfunction()
