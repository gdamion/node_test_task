option(ENABLE_PCH "Enable Precompiled Headers" ON)

if(ENABLE_PCH)
  if(CMAKE_CXX_COMPILER_ID MATCHES "Clang")
    set(CMAKE_PCH_INTANTIATE_TEMPLATES ON)
  endif()
endif()
