option(ENABLE_UNITY "Enable Unity build method of binaries" ON)

if(ENABLE_UNITY)
  set(CMAKE_UNITY_BUILD ON)
  set(CMAKE_UNITY_BUILD_BATCH_SIZE 5)
endif()
