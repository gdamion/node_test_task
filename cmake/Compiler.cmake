macro(SetCXXCompiler compiler)
  set(CMAKE_CXX_COMPILER compiler)
  message(
    STATUS "C++ compiler: ${CMAKE_CXX_COMPILER_ID}, ${CMAKE_CXX_COMPILER}")
endmacro()

macro(SetCCompiler compiler)
  set(CMAKE_C_COMPILER compiler)
  message(STATUS "C compiler: ${CMAKE_C_COMPILER_ID}, ${CMAKE_C_COMPILER}")
endmacro()

# set(CMAKE_STANDARD_LIBRARIES "" CACHE STRING "" FORCE)
# set(CMAKE_C_STANDARD_LIBRARIES "" CACHE STRING "" FORCE)
# set(CMAKE_CXX_STANDARD_LIBRARIES "" CACHE STRING "" FORCE)
