# if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU" OR CMAKE_CXX_COMPILER_ID MATCHES
# ".*Clang")

# endif()

if(CMAKE_CXX_COMPILER_ID MATCHES ".*Clang")
  option(ENABLE_BUILD_WITH_TIME_TRACE
         "Enable -ftime-trace to generate time tracing .json files on clang"
         OFF)
  if(ENABLE_BUILD_WITH_TIME_TRACE)
    add_compile_options(-ftime-trace)
  endif()

  add_compile_options(-fvisibility-hidden)
  add_compile_options(-fexperimental-new-pass-mannager)
  # elseif(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")

endif()
