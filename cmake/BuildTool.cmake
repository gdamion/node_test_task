option(ENABLE_NINJA "Enable Ninja build tool" ON)
if(NOT ENABLE_NINJA OR CMAKE_GENERATOR)
  return()
endif()

find_program(NINJA_EXECUTABLE ninja)

if(NINJA_EXECUTABLE)
  set(CMAKE_GENERATOR "Ninja")
  add_definitions(-DCOMPILER=ninja)
  message(STATUS "${CMAKE_GENERATOR} is set as build tool")
endif()
